//Button link
function join()
{
    window.open("classes.html")
}
function know() {
  window.open("about.html")
}
function contact() 
{
    window.open("contact.html")
}
//Scroll tint
window.addEventListener('scroll', function() {
    var navbar = document.querySelector('.navbar');
    if(window.scrollY > 0) {
        navbar.classList.add('navbar-scrolled');
    } else {
        navbar.classList.remove('navbar-scrolled');
    }
});

//Google tag (gtag.js)
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'G-GNT2HBZZMR');

    // Smooth scrolling
    document.querySelectorAll('.icon-scroll').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });
    
    // JavaScript to remove fixed background attachment after scrolling past the section
    window.addEventListener('scroll', function() {
        var section = document.getElementById('contact');
        var backdrop = section.querySelector('.backdrop');
        if (window.scrollY > section.offsetTop + section.offsetHeight) {
            backdrop.style.backgroundAttachment = 'scroll';
        } else {
            backdrop.style.backgroundAttachment = 'fixed';
        }
    });
    function initMap() {
        var mapOptions = {
            center: { lat: 13.0088469, lng: 77.5644782 }, // Set the center of the map
            zoom: 17 // Set the zoom level
        };
        var map = new google.maps.Map(document.getElementById('map'), mapOptions); // Create the map object
    }
// JavaScript to toggle navbar background color on scroll
window.addEventListener('scroll', function() {
    var navbar = document.querySelector('.navbar-custom');
    if (window.scrollY > 50) {
        navbar.classList.add('navbar-scrolled');
    } else {
        navbar.classList.remove('navbar-scrolled');
    }
});